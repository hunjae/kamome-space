import React, { Component } from 'react';
 
// Task component - represents a single todo item
export default class LocationInfo extends Component {
  nNavi() {
    var lat = 37.550821;
    var lon = 126.943948;
    var destination = '서강대학교 후문';
    window.open("https://m.map.naver.com/directions/?"+'ename='+destination+"&ex="+lon+"&ey="+lat);
  }

  tmap() {
    var lat = 37.550821;
    var lon = 126.943948;
    var destination = '서강대학교 후문';
    window.open("http://m.tmap.co.kr/tmap2/mobile/route.jsp?"+'name='+destination+"&lon="+lon+"&lat="+lat);
  }

  kNavi() {
    // 공통사항 설정
    var lat = 37.550821;
    var lon = 126.943948;
    var destination = '서강대학교 후문';
    Kakao.Navi.start({ name: destination, x: lon, y: lat, coordType: 'wgs84'})
  }

  render() {
    return (
        <div className="locationInfo">
          <div id="map" className="map"></div>
          <img src="images/inv2_08.jpg"/>
          <div className="navigation">
            <img className="naviTitle" src="./images/navi-title.png"/>
                <img id="btnNNavi" src="./images/navi-n.png" onClick={this.nNavi}/>
                <img id="btnTmap" src="./images/navi-t.png" onClick={this.tmap}/>
                <img id="btnKNavi" src="./images/navi-k.png" onClick={this.kNavi}/>
          </div>
          <img src="images/locationInfo.jpg"/>
        </div>
    );
  }

  componentDidMount() {
    /*경도위도 마커이름*/
    var coordinateX = 37.551050;
    var coordinateY = 126.943218;
    var weddingName = '곤자가컨벤션';

    /*네이버mapV3 및 마커찍기*/
    var weddinghall = new naver.maps.LatLng(coordinateX, coordinateY),
        map = new naver.maps.Map('map', {
            center: weddinghall.destinationPoint(0, 100),
            scaleControl: false,
            logoControl: true,
            mapDataControl: false,
            zoomControl: true,
            minZoom: 1
        }),
        marker = new naver.maps.Marker({
            map: map,
            position: weddinghall
        });

    /*말풍선꾸미기*/
    var contentString = [
            '<div class="iw_inner">',
            '   <p id="weddingNm">'+weddingName+'</p>',
            '</div>'
        ].join('');

    var infowindow = new naver.maps.InfoWindow({
        content: contentString
    });
    naver.maps.Event.addListener(marker, "click", function(e) {
        if (infowindow.getMap()) infowindow.close();
        else infowindow.open(map, marker);
    });
    infowindow.open(map, marker);
  }
}
