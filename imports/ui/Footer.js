import React, { Component } from 'react';
 
// Task component - represents a single todo item
export default class Footer extends Component {
  render() {
    return (
        <div className="footer" onClick={this.kakaoLink}>
            <img id="linkImg" src="images/kakao.png" />
            <img id="linkText" src="images/link-text.png" />
        </div>
    );
  }

  kakaoLink() {
      Kakao.Link.sendCustom({
        templateId: 8366,
        templateArgs: {
          'title': '헌재와 서희, 하나되는 봄날',
          'desc': '일시 : 2018. 04. 14 (토) 12:00\n장소 : 서강대 곤자가컨벤션',
          'btn': '청첩장 보러가기',
          'url': '/1804/0414_jhjhsh', //도메인url빼고
          'img': 'http://inv.gensdesign.co.kr/1804/0414_jhjhsh/images/main.jpg'//전체url포함 //세로사이즈400px 고정, 가로는 400px이상
          }
      });
  }
}
